//: Playground - noun: a place where people can play

import UIKit

//variables
var str = "Hello, playground"
let flash = "flash"
let bang: Character = "!"
let crash = flash

//bool
let powerOn = false
let keyInDoor = true
let doorOpened = keyInDoor && powerOn

//array
let names = ["Alice", "Bob","Carol"];
var cities: [String]
var states: Array<String>

//dictionary
let abcAges = ["Alice":24,"Bob":25,"Carol":29];
var ages: Dictionary<String,Int>;
ages = abcAges;
ages["Bob"] = 14;
ages;

//conditionals
if(ages["Bob"] == 14){
    ages["Bob"] = 18;
}
ages;

//loops
for index in 1...10{
    print("index is \(index)");
}
for index in 1..<10{
    print("index is \(index)");
}

//tuple
for(name,age) in ages{
    print("\(name) is \(age) years old.");
}